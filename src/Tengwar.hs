{-# LANGUAGE OverloadedStrings #-}

module Tengwar where

import qualified Text.Blaze.Html5 as H
import Text.Blaze.Html5 ((!))
import qualified Text.Blaze.Html5.Attributes as A
import qualified Data.Text.Lazy as T


-- | Converts strings to tengwarjs format
tengwar :: T.Text -> H.Html
tengwar s = H.div
            ! A.class_ "tengwar parmaite"
            ! H.dataAttribute "mode" "classical"
            ! H.dataAttribute "tengwar" (H.toValue s)
            $ ""

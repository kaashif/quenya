Quenya Web App
==============
If you want to learn Quenya, and you're a hip, radical youngster
looking to do it on the world wide web, you've come to the right
place. Just install cabal, install happstack-lite, and get cracking.

Alternatively, visit the instance I have running
[here](http://quenya.herokuapp.com/).

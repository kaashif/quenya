{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad
import qualified Data.Text as T
import Happstack.Server
import Text.Blaze.Html5 ((!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import System.Environment (getEnvironment)
import qualified Data.Map as M
import Data.Maybe (fromMaybe)
import Tengwar (tengwar)

main :: IO ()
main = do
  env <- getEnvironment
  let myPort = fromMaybe "8080" $ M.lookup "PORT" (M.fromList env)
  simpleHTTP (nullConf { port = (read myPort) :: Int }) $ do
    decodeBody (defaultBodyPolicy "/tmp/" 4096 4096 4096)
    myApp

myApp :: ServerPart Response
myApp = msum
  [ dir "static" $ serveDirectory EnableBrowsing [""] "./static"
  , dir "about" $ aboutPage
  , dir "test" $ testPage
  , dir "resources" $ resourcesPage
  , dir "dictionary" $ dictPage
  , homePage
  ]

template :: T.Text -> H.Html -> Response
template title content = toResponse $
  H.html $ do
    H.head $ do
     H.title (H.toHtml title)
     H.link ! A.rel "stylesheet" ! A.href "/static/bootstrap.min.css"
     H.link ! A.rel "stylesheet" ! A.href "/static/extra.css"
     H.link ! A.rel "stylesheet" ! A.href "/static/tengwar-parmaite.css"
     H.script ! A.type_ "text/javascript" ! A.src "/static/tengwar.min.js" $ ""
    H.body $ do
      H.nav ! A.class_ "navbar navbar-inverse navbar-fixed-top" $ do
        H.div ! A.id "navbar" $ do
          H.a ! A.class_ "navbar-brand" ! A.href "/" $ "Parma Quenyanna"
          H.ul ! A.class_ "navbar-nav nav" $ do
            H.li $ H.a ! A.href "/" $ "Home"
            H.li $ H.a ! A.href "/test" $ "Test Yourself"
            H.li $ H.a ! A.href "/dictionary" $ "Dictionary"
            H.li $ H.a ! A.href "/about" $ "About"

      H.div ! A.class_ "container" $
        H.div ! A.class_ "content" $ content

homePage :: ServerPart Response
homePage =
    ok $ template "Parma Quenyanna" $
      H.div ! A.class_ "jumbotron" $
        H.div ! A.class_ "container" $ do
          H.h1 "Utúlien aurë!"
          H.p "The day to learn Quenya has come. It's not that hard, and you'll be a lot more fun at parties, trust me."
          H.p $ H.a ! A.class_ "btn btn-primary btn-lg" ! A.href "/resources" $ "Get started"
        

aboutPage :: ServerPart Response
aboutPage =
    ok $ template "About Quenya" $ do
      H.h1 "About Quenya"
      H.p "Quenya is a real language, and don't let anyone tell you otherwise."

testPage :: ServerPart Response
testPage =
    ok $ template "Test your Quenya" $ do
      H.h1 "Test your Quenya"

resourcesPage :: ServerPart Response
resourcesPage =
    ok $ template "Quenya Learning Resources" $ do
      H.h1 "Some useful resources for learning Quenya"
      H.p $ do
        "I got started with Quenya "
        H.a ! A.href "http://folk.uib.no/hnohf/qcourse.htm" $ "here"
        ". That guy really knows what he's talking about, don't be put off by the slightly dated look of the site."

dictPage :: ServerPart Response
dictPage = msum [ submitForm, respondForm ]
    where
      submitForm :: ServerPart Response
      submitForm =
          do method GET
             ok $ template "Quenya Dictionary" $ do
                H.h1 "English/Quenya Dictionary"
                H.form ! A.action "/dictionary" ! A.enctype "multipart/form-data" ! A.method "POST" $ do
                  H.input ! A.type_ "text" ! A.id "word" ! A.class_ "form-control" ! A.name "word"
                  H.input ! A.type_ "submit" ! A.class_ "btn" ! A.value "Look it up!"

      respondForm :: ServerPart Response
      respondForm =
          do method POST
             word <- lookText "word"
             ok $ template "Quenya Dictionary" $ do
               H.div ! A.class_ "container" $ do
                 H.h2 "You typed:"
                 H.toHtml word
                 tengwar word
               H.div ! A.class_ "col-md-6" $ do
                 H.h3 "Closest English match:"
                 H.p "ayy (noun) - a group of lmaos"
               H.div ! A.class_ "col-md-6" $ do
                 H.h3 "Closest Quenya match:"
                 tengwar "ayy lmao"
                 H.p "ayy (verb) - to meme"
               
 
